﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Exemplos_Metodos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int v1;
            v1 = int.Parse(txtValor1.Text);
            txtRes.Text = Metodos.calcularQuadrado(v1).ToString();
        }

        /*private int calcularQuadrado(int valor)
        {
            return valor * valor;
        }*/
    }
}
